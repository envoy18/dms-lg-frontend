import { Component, OnInit, Renderer2, Inject, ElementRef, ViewChild } from '@angular/core';
import { DOCUMENT, CurrencyPipe, DatePipe } from '@angular/common';
import { ExpensesService } from 'src/app/shared/expenses.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AdditionalFilesService } from 'src/app/shared/additional-files.service';
import { NgForm } from '@angular/forms';

declare var $;
@Component({
  selector: 'app-dms-expenses-files',
  templateUrl: './dms-expenses-files.component.html',
  styleUrls: ['./dms-expenses-files.component.css']
})
export class DmsExpensesFilesComponent implements OnInit {
  expenses_id;
  expenses:any = [];
  expensesFiles =[];
  fileUrl;
  loginUser;
  selectedFile:File;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public expensesService: ExpensesService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private currencyPipe: CurrencyPipe,
    private datePipe:DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loginUser = localStorage.getItem('loginUser');
    const component = this;
    this.expensesService.refreshList();
    this.additionalFilesService.refreshListForExpenses();
    this.expenses_id = this.route.snapshot.paramMap.get('id');
    this.expensesService.getExpensesByID(this.expenses_id).subscribe((res)=>{
            this.expenses = res;
            this.expenses.id
            
        });
    this.getExpensesFilesById(this.expenses_id);

    $('#filesTable').on('click', '#deleteFiles', function(){
      var id = $(this).data('id');
      component.onDelete(id);
    })
  }

  populateForm(expenses:any){
    this.expensesService.formData = Object.assign({}, this.expenses)
    this.expensesService.additionalFiles = [];
    this.loadSelectedFiles()
    
  }

  getExpensesFilesById(id){
    this.additionalFilesService.refreshListForExpenses();
    const component = this;
    setTimeout(function(){
      var expensesFiles = component.additionalFilesService.additionalFilesListExpenses
      for(var i = 0; i < expensesFiles.length; i ++){
        if(expensesFiles[i].files_Id == id){
          component.expensesFiles.push(expensesFiles[i])
          
        }
      }
    },1000)
    
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record? " + id)){
      this.reloadFiles();
      this.additionalFilesService.deleteExpensesAdditionalFileById(id).subscribe(
        res => {
          this.additionalFilesService.refreshListForExpenses();
          this.toastr.warning('Deleted successfully');
          
      },
      err =>{
        console.log(err);
      });;
      
    }
  }

  reloadFiles(){
    this.expensesFiles=[];
    this.getExpensesFilesById(this.expenses_id);
    const component = this;
    setTimeout(function(){
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var expensesFiles = component.expensesFiles;
      var tr = '';
      var filePerUrl = []
      for(var i = 0; i < expensesFiles.length; i ++){
        filePerUrl[i] = component.downloadFile(expensesFiles[i])
        tr = tr + 
            "<tr>" +
              "<td>" + expensesFiles[i].fileName + "</td>" +
              "<td>" +
                  "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + expensesFiles[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                      "<img src='../assets/images/download.png' width='15' height='15'>" +
                  "</a>" +
                  "<button type='button' class='btn btn-sm btn-danger' id='deleteFiles' data-id='"+expensesFiles[i].id+"'>" +
                      "<img src='../assets/images/delete.png' width='15' height='15'>" +
                  "</button>"+
                "</td>" +
              "</tr>"

      }
      $('#filesTable').append(tableBodyOpen + tr + tableBodyClose);
    },1000)
  }

  downloadFile(expensesFile){
    var binaryString = '';
    for(var a = 0; a < expensesFile.fileBinary.length; a++) {
      binaryString = binaryString + expensesFile.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  updateRecord(form:NgForm){
    this.expensesService.formData.updatedBy = this.loginUser;
    this.expensesService.formData.description = this.expenses.description;
    this.expensesService.formData.amount = this.expenses.amount;
    console.log(this.expensesService.formData);

    this.expensesService.updateExpenses().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';

        this.toastr.info('Updated successfully','File Updated')
        // this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadFiles()
        this.expensesService.additionalFiles = [];
        this.loadSelectedFiles()
      },
      err => {
        console.log(err);
      }
    );
  }
  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    }
  }
  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile)
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "Expenses";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.expensesService.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.expensesService.additionalFiles)

    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.expensesService.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.expensesService.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRow'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRow',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.expensesService.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.expensesService.additionalFiles);
      component.loadSelectedFiles();
    });
  }
}
