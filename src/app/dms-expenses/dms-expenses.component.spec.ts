import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmsExpensesComponent } from './dms-expenses.component';

describe('DmsExpensesComponent', () => {
  let component: DmsExpensesComponent;
  let fixture: ComponentFixture<DmsExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmsExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmsExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
