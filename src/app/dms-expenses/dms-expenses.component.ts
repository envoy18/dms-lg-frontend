import { Component, OnInit, Renderer2, Inject, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT, CurrencyPipe, DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { ExpensesService } from '../shared/expenses.service';
import { ToastrService } from 'ngx-toastr';
import { Expenses } from '../shared/expenses.model';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AdditionalFilesService } from '../shared/additional-files.service';
declare var $;

@Component({
  selector: 'app-dms-expenses',
  templateUrl: './dms-expenses.component.html',
  styleUrls: ['./dms-expenses.component.css']
})
export class DmsExpensesComponent implements OnInit {
  date : Date = new Date();
  todaymm = ((this.date.getMonth() + 1) < 10 ? '0' : '') + (this.date.getMonth() + 1);
  todaydd = (this.date.getDate() < 10 ? '0' : '') + (this.date.getDate(),1);
  todayyy = this.date.getFullYear();

  lastDatemm = ((this.date.getMonth() + 1) < 10 ? '0' : '') + (this.date.getMonth() + 1);
  lastDatedd = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).toString().substr(8,2); 
  lastDateyy = this.date.getFullYear();
  
  formattedtodayDate : string = this.todayyy + '-' + this.todaymm + '-' + '01';
  formattedlastDate : string = this.lastDateyy + '-' + this.lastDatemm + '-' + this.lastDatedd;
  startDate;
  endDate;

  expensesList: Expenses[];

  selectedFile:File = null;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  @ViewChild('dataTable', {static: false}) dataTable: ElementRef;
  fileUrl;
  
  loginUser;

  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public service: ExpensesService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private currencyPipe: CurrencyPipe,
    private datePipe:DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService) { } 
    
    
    ngOnInit() {
      if(localStorage.getItem('loginUser') == null){
        this.router.navigate(['login']);
      }  
      $(document).ready(function() {
        setTimeout(function(){
          $('#dataTableExpenses').DataTable({
            "order": [[ 2,'desc']]
          });
        }, 500)
      });
      this.resetForm();
      this.service.formDataDateFilter.startDate =  this.formattedtodayDate;
      this.service.formDataDateFilter.endDate =  this.formattedlastDate;
      this.service.refreshListByDate();
      this.removeSelectedFile();
      console.log(this.formattedtodayDate)
      console.log(this.formattedlastDate)
      this.loginUser = localStorage.getItem('loginUser');
      const component = this;
      $('#dataTableExpenses').on('click', '#deleteData', function(){
        var id = $(this).data('id');
        component.onDelete(id);
      }) ;

      $('#dataTableExpenses').on('click', '#updateData', function(){
        var expenses_id = $(this).data('id');
        var expenses = component.service.listByDate.find(x => x.id == expenses_id)
        component.populateForm(expenses);
      }) ;

      $('#dataTableExpenses').on('click','#navigateToFiles', function(){
        var expenses_id = $(this).data('id');
        component.navigateToFiles(expenses_id);
      })
    }
    
    resetForm(form?: NgForm){
      if(form!=null)
        form.resetForm();
      this.service.formData = {
        id: 0,
        fileName:'',
        fileSize: 0,
        fileType:'',
        description:'',
        fileBinary:[],
        amount: null,
        dateCreated: new Date(),
        dateUpdated: new Date(),
        createdBy:'',
        updatedBy:'',
      };
      this.service.formDataDateFilter = {
        startDate: '',
        endDate: ''
      };
      this.additionalFilesService.formData = {
        id: 0,
        category:'',
        fileName:'',
        fileSize: 0,
        fileType:'',
        description:'',
        fileBinary:[],
        dateCreated: new Date(),
        createdBy:'',
        files_Id:0
      }
    }
    
    onFileChange(event){
      this.resetForm();
      this.selectedFile = <File>event.target.files[0];
      this.additionalFilesService.formData.fileName = this.selectedFile.name;
      this.additionalFilesService.formData.fileSize = this.selectedFile.size;
      this.additionalFilesService.formData.fileType = this.selectedFile.type;
      this.additionalFilesService.formData.category = "Expenses";
      this.additionalFilesService.formData.createdBy = this.loginUser;
      var reader = new FileReader();
      var fileByteArray = [];
      reader.readAsArrayBuffer(this.selectedFile);
      reader.onload = function (evt:any) {
        if (evt.target.readyState == FileReader.DONE) {
          var arrayBuffer = evt.target.result,
               array = new Uint8Array(arrayBuffer);
          for (var i = 0; i < array.length; i++) {
               fileByteArray.push(array[i]);
          }
        }
      }
      this.additionalFilesService.formData.fileBinary = fileByteArray;
      this.service.additionalFiles.push(this.additionalFilesService.formData)
      console.log(this.service.additionalFiles)

      this.loadSelectedFiles();
    }

    loadSelectedFiles(){
      $('#removeAll').remove();    
      var content ='';            
      for(var x = 0 ; x < this.service.additionalFiles.length; x++){
         content = content +
                "<div class='row' id='tableRow'>" +
                  "<div class='col-md-10' >"+
                     this.service.additionalFiles[x].fileName + 
                  "</div>" +
                  "<div class='col'>"+
                      "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRow'>&#8722;</button>"+
                  "</div>"+
                "</div>";
      }
      $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
      
    }

    removeSelectedFile(){
      const component = this;
      $('body').on('click','#selectedFileRow',function(){
        // $(this).closest('#tableRow').remove();
        var id = $(this).data('id');
        component.service.additionalFiles.splice(id,1);
        console.log(id);
        console.log(component.service.additionalFiles);
        component.loadSelectedFiles();
      });
    }
    onSubmit(form:NgForm){
      if(this.service.formData.id == 0){
        this.insertRecord(form);
      }else{
        this.updateRecord(form);
      }
    }
  
    insertRecord(form:NgForm){
      this.service.formData.createdBy = this.loginUser;
      this.service.formData.updatedBy = this.loginUser;
      this.service.addExpenses().subscribe(
        res => {
          this.fileInput.nativeElement.value = '';
          this.resetForm(form);
          this.toastr.success('Submitted successfully','New File Uploaded')
          // this.service.refreshList();
          this.closeModal.nativeElement.click();
          this.DateChanged();

        },
        err => {
          console.log(err);
        }
      );
    }
  
    updateRecord(form:NgForm){
      this.service.formData.updatedBy = this.loginUser;
      this.service.updateExpenses().subscribe(
        res => {
          this.fileInput.nativeElement.value = '';
          this.resetForm(form);
          this.toastr.info('Updated successfully','File Updated')
          // this.service.refreshList();
          this.closeModal.nativeElement.click();
          this.DateChanged();
        },
        err => {
          console.log(err);
        }
      );
    }

    populateForm(expenses:Expenses){
      this.clearInputFile()
      this.service.formData = Object.assign({}, expenses);
    }

    clearInputFile(){
      this.fileInput.nativeElement.value = '';
      this.service.additionalFiles = [];
      $('#removeAll').remove();    
    }

    clearForm(){
      this.resetForm();
      this.clearInputFile()   
    }

    onDelete(id){
      if(confirm("Are you sure to delete this record? " + id)){
        console.log(id)
        this.service.deleteExpenses(id).subscribe(
          res => {
            // this.service.refreshList();
            this.toastr.warning('Deleted successfully');
            this.DateChanged();
        },
        err =>{
          console.log(err);
        });;
      }
    }

    downloadFile(expenses){
      var binaryString = '';
      for(var a = 0; a < expenses.fileBinary.length; a++) {
        binaryString = binaryString + expenses.fileBinary[a];
      }
      var binary = atob(binaryString);
      var array = [];
      for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      var bytes = new Uint8Array(array);
      var blob = new Blob([bytes], { type: 'application/octet-stream' });
      this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
      return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    }

    DateChanged(){
      this.service.formDataDateFilter.startDate= this.formattedtodayDate;
      this.service.formDataDateFilter.endDate= this.formattedlastDate;
      this.service.refreshListByDate();
      this.reloadDataTable();
      // this.LoadDataTable();
    }

    reloadDataTable(){
      const component = this;
      setTimeout(function() { 
        $('#dataTableExpenses').DataTable().destroy()
        $('tbody').remove();
        var tableBodyOpen = "<tbody>";
        var tableBodyClose = "</tbody>"
        var expenses = component.service.listByDate;
        var tr = '';
        var filePerUrl = [];
        
        for(var i = 0; i < expenses.length; i ++){
          var formattedCurrency = component.currencyPipe.transform(expenses[i].amount, 'PHP');
          var formattedDate = component.datePipe.transform(expenses[i].dateUpdated, 'yyyy/MM/dd')
          filePerUrl[i] = component.downloadFile(expenses[i])
          tr = tr +
            "<tr>" +
              // "<td> " + expenses[i].fileName + "</td>" +
              "<td>" + expenses[i].description + "</td>" +
              "<td>" + formattedCurrency + "</td>" +
              "<td>" + formattedDate + "</td>" + 
              "<td>" +
                  // "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + expenses[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                  //     "<i class='fas fa-download'></i>" +
                  // "</a>" +
                  "<a  id='navigateToFiles' data-id='"+ expenses[i].id +"' routerLinkActive='active' class='btn btn-sm btn-info' ><img src='../assets/images/view.png' width='15' height='15'></a>" +
                  // "<button type='button' class='btn btn-sm btn-warning' data-toggle='modal' data-target='#exampleModal' id='updateData' data-id='" + expenses[i].id + "'>" +
                  //     "<i class='far fa-edit fa-lg' ></i>" +
                  // "</button>" +
                  "<button type='button' class='btn btn-sm btn-danger' id='deleteData' data-id='" + expenses[i].id + "'>" +
                      "<img src='../assets/images/delete.png' width='15' height='15'>" +
                  "</button>" +
              "</td>" +
            "</tr>"
          
            // onclick='" + this.onDelete(expenses[i].id) + "'
          
        }

        $('#dataTableExpenses').append(tableBodyOpen + tr + tableBodyClose); 
        $('#dataTableExpenses').DataTable({
          "order": [[ 2,'desc']]
        })
      }, 1000);
    }

    navigateToFiles(id){
      this.router.navigate(['Expenses',id, 'Files']);
    }
  }
