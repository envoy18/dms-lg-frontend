import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { MatMenuModule, MatStepperModule, MatInputModule, MatButtonModule, MatGridListModule, MatFormFieldModule, MatRadioModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatCheckboxModule} from '@angular/material';
import { DmsExpensesComponent } from './dms-expenses/dms-expenses.component';
import { DmsLetterComponent } from './dms-letter/dms-letter.component';
import { DmsSalesreportComponent } from './dms-salesreport/dms-salesreport.component'
import { DataTablesModule } from 'angular-datatables';
import { LetterService } from './shared/letter.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SalesReportService } from './shared/sales-report.service';

import { DmsInsurancePermitsComponent } from './dms-insurance-permits/dms-insurance-permits.component';
import { InsurancePermitsService } from './shared/insurance-permits.service';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { ExpensesService } from './shared/expenses.service';
import { DateDiffPipe } from './pipe/date-diff.pipe';
import { DmsOthersComponent } from './dms-others/dms-others.component';
import { LoginComponent } from './user/login/login.component';
import { AdditionalFilesService } from './shared/additional-files.service';
import { DmsExpensesFilesComponent } from './dms-expenses/dms-expenses-files/dms-expenses-files.component';
import { DmsLetterFilesComponent } from './dms-letter/dms-letter-files/dms-letter-files.component';
import { DmsSalesreportFilesComponent } from './dms-salesreport/dms-salesreport-files/dms-salesreport-files.component';
import { DmsInsurancePermitsFilesComponent } from './dms-insurance-permits/dms-insurance-permits-files/dms-insurance-permits-files.component';
import { DmsOthersFilesComponent } from './dms-others/dms-others-files/dms-others-files.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    DmsExpensesComponent,
    DmsLetterComponent,
    DmsSalesreportComponent,
    DmsInsurancePermitsComponent,
    DateDiffPipe,
    DmsOthersComponent,
    LoginComponent,
    DmsExpensesFilesComponent,
    DmsLetterFilesComponent,
    DmsSalesreportFilesComponent,
    DmsInsurancePermitsFilesComponent,
    DmsOthersFilesComponent,

   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatStepperModule, 
    MatInputModule, 
    MatButtonModule,
    MatGridListModule,
    MatFormFieldModule, 
    MatRadioModule, 
    MatDatepickerModule, 
    MatNativeDateModule, 
    MatSelectModule,
    MatCheckboxModule,
    MatMenuModule,
    DataTablesModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
      
  ],
  providers: [
    LetterService,
    InsurancePermitsService,
    DatePipe,
    SalesReportService,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ExpensesService,
    CurrencyPipe,
    DatePipe,
    AdditionalFilesService
    
  ],
 
  bootstrap: [AppComponent]
})
export class AppModule { }
