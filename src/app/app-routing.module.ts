import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { DmsExpensesComponent } from './dms-expenses/dms-expenses.component';
import { DmsLetterComponent } from './dms-letter/dms-letter.component';
import { DmsSalesreportComponent } from './dms-salesreport/dms-salesreport.component';
import { DmsInsurancePermitsComponent } from './dms-insurance-permits/dms-insurance-permits.component';
import { DmsOthersComponent } from './dms-others/dms-others.component';
import { LoginComponent } from './user/login/login.component';
import { DmsExpensesFilesComponent } from './dms-expenses/dms-expenses-files/dms-expenses-files.component';
import { DmsLetterFilesComponent } from './dms-letter/dms-letter-files/dms-letter-files.component';
import { DmsSalesreportFilesComponent } from './dms-salesreport/dms-salesreport-files/dms-salesreport-files.component';
import { DmsInsurancePermitsFilesComponent } from './dms-insurance-permits/dms-insurance-permits-files/dms-insurance-permits-files.component';
import { DmsOthersFilesComponent } from './dms-others/dms-others-files/dms-others-files.component';

const routes: Routes = [
  {path: '', redirectTo: 'login',pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {
    path: '', 
    component: LayoutComponent,
    children:[
      {path: 'Expenses', component: DmsExpensesComponent},
      {path: 'Letter', component: DmsLetterComponent},
      {path: 'SalesReport', component: DmsSalesreportComponent},
      {path: 'InsurancePermits', component: DmsInsurancePermitsComponent},
      {path: 'Others', component: DmsOthersComponent},
      {path: 'Expenses/:id/Files', component: DmsExpensesFilesComponent},
      {path: 'Letter/:id/Files', component: DmsLetterFilesComponent},
      {path: 'SalesReport/:id/Files', component: DmsSalesreportFilesComponent},
      {path: 'InsurancePermits/:id/Files', component: DmsInsurancePermitsFilesComponent},
      {path: 'Others/:id/Files', component: DmsOthersFilesComponent},
      
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [DmsExpensesComponent,
                                  DmsLetterComponent,
                                  DmsSalesreportComponent,
                                  DmsInsurancePermitsComponent,
                                  DmsOthersComponent,
                                  DmsExpensesFilesComponent,
                                  DmsLetterFilesComponent,
                                  DmsSalesreportFilesComponent,
                                  DmsInsurancePermitsFilesComponent,
                                  DmsOthersFilesComponent]
