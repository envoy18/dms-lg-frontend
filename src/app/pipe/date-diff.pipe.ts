import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateDiff'
})
export class DateDiffPipe implements PipeTransform {

  transform(date1: any, date2?: any): any {
    const dayDiff = moment(date1).diff(moment(date2), "days");
    return Math.abs(dayDiff);
  }

}
