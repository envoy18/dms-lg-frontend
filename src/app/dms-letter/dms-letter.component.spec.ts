import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmsLetterComponent } from './dms-letter.component';

describe('DmsLetterComponent', () => {
  let component: DmsLetterComponent;
  let fixture: ComponentFixture<DmsLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmsLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmsLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
