import { Component, OnInit, Renderer2, Inject, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT, CurrencyPipe, DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AdditionalFilesService } from 'src/app/shared/additional-files.service';
import { LetterService } from 'src/app/shared/letter.service';
import { NgForm } from '@angular/forms';
declare var $;
@Component({
  selector: 'app-dms-letter-files',
  templateUrl: './dms-letter-files.component.html',
  styleUrls: ['./dms-letter-files.component.css']
})
export class DmsLetterFilesComponent implements OnInit {
  letter_id;
  letter:any = [];
  letterFiles =[];
  fileUrl;
  loginUser;
  selectedFile:File;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public letterService: LetterService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private currencyPipe: CurrencyPipe,
    private datePipe:DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loginUser = localStorage.getItem('loginUser');
    const component = this;
    this.letterService.refreshList();
    this.additionalFilesService.refreshListForLetters();
    this.letter_id = this.route.snapshot.paramMap.get('id');
    this.letterService.getLetterByID(this.letter_id).subscribe((res)=>{
            this.letter = res;
            this.letter.id
            
        });
    this.getLetterFilesById(this.letter_id);
    this.removeSelectedFile();
    $('#filesTable').on('click', '#deleteFiles', function(){
      var id = $(this).data('id');
      component.onDelete(id);
    })
  }

  populateForm(letter:any){
    this.letterService.formData = Object.assign({}, this.letter)
    this.letterService.additionalFiles = [];
    this.loadSelectedFiles()
    
  }

  getLetterFilesById(id){
    this.additionalFilesService.refreshListForLetters();
    const component = this;
    setTimeout(function(){
      var letterFiles = component.additionalFilesService.additionalFilesListLetters
      for(var i = 0; i < letterFiles.length; i ++){
        if(letterFiles[i].files_Id == id){
          component.letterFiles.push(letterFiles[i])
          
        }
      }
      console.log(component.letterFiles)
    },1000)
    
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record? " + id)){
      this.reloadFiles();
      this.additionalFilesService.deleteLettersAdditionalFileById(id).subscribe(
        res => {
          this.additionalFilesService.refreshListForLetters();
          this.toastr.warning('Deleted successfully');
          
      },
      err =>{
        console.log(err);
      });;
      
    }
  }

  reloadFiles(){
    this.letterFiles=[];
    this.getLetterFilesById(this.letter_id);
    const component = this;
    setTimeout(function(){
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var letterFiles = component.letterFiles;
      var tr = '';
      var filePerUrl = []
      for(var i = 0; i < letterFiles.length; i ++){
        filePerUrl[i] = component.downloadFile(letterFiles[i])
        tr = tr + 
            "<tr>" +
              "<td>" + letterFiles[i].fileName + "</td>" +
              "<td>" +
                  "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + letterFiles[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                      "<img src='../assets/images/download.png' width='15' height='15'>" +
                  "</a>" +
                  "<button type='button' class='btn btn-sm btn-danger' id='deleteFiles' data-id='"+letterFiles[i].id+"'>" +
                      "<img src='../assets/images/delete.png' width='15' height='15'>" +
                  "</button>"+
                "</td>" +
              "</tr>"

      }
      $('#filesTable').append(tableBodyOpen + tr + tableBodyClose);
    },1000)
  }

  downloadFile(letterFile){
    var binaryString = '';
    for(var a = 0; a < letterFile.fileBinary.length; a++) {
      binaryString = binaryString + letterFile.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  updateRecord(form:NgForm){
    this.letterService.formData.updatedBy = this.loginUser;
    this.letterService.formData.description = this.letter.description;
    console.log(this.letterService.formData);

    this.letterService.updateLetter().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';

        this.toastr.info('Updated successfully','File Updated')
        // this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadFiles()
        this.letterService.additionalFiles = [];
        this.loadSelectedFiles()
      },
      err => {
        console.log(err);
      }
    );
  }
  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    }
  }
  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile)
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "Letters";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.letterService.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.letterService.additionalFiles)

    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.letterService.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.letterService.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRowLetters'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRowLetters',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.letterService.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.letterService.additionalFiles);
      component.loadSelectedFiles();
    });
  }

}
