import { Component, OnInit, ViewChild, ElementRef, Renderer2, Inject } from '@angular/core';
import { DOCUMENT, CurrencyPipe, DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AdditionalFilesService } from 'src/app/shared/additional-files.service';
import { OthersService } from 'src/app/shared/others.service';
import { NgForm } from '@angular/forms';
declare var $;
@Component({
  selector: 'app-dms-others-files',
  templateUrl: './dms-others-files.component.html',
  styleUrls: ['./dms-others-files.component.css']
})
export class DmsOthersFilesComponent implements OnInit {
  others_id;
  others:any = [];
  othersFiles =[];
  fileUrl;
  loginUser;
  selectedFile:File;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public othersService: OthersService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private currencyPipe: CurrencyPipe,
    private datePipe:DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loginUser = localStorage.getItem('loginUser');
    const component = this;
    this.othersService.refreshList();
    this.additionalFilesService.refreshListForOthers();
    this.others_id = this.route.snapshot.paramMap.get('id');
    this.othersService.getOthersByID(this.others_id).subscribe((res)=>{
            this.others = res;
            this.others.id
            
        });
    this.getOthersFilesById(this.others_id);
    this.removeSelectedFile();
    $('#filesTable').on('click', '#deleteFiles', function(){
      var id = $(this).data('id');
      component.onDelete(id);
    })
  }

  populateForm(others:any){
    this.othersService.formData = Object.assign({}, this.others)
    this.othersService.additionalFiles = [];
    this.loadSelectedFiles()
    
  }

  getOthersFilesById(id){
    this.additionalFilesService.refreshListForOthers();
    const component = this;
    setTimeout(function(){
      var othersFiles = component.additionalFilesService.additionalFilesListOthers
      for(var i = 0; i < othersFiles.length; i ++){
        if(othersFiles[i].files_Id == id){
          component.othersFiles.push(othersFiles[i])
          
        }
      }
      console.log(component.othersFiles)
    },1000)
    
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record? " + id)){
      this.reloadFiles();
      this.additionalFilesService.deleteOthersAdditionalFileById(id).subscribe(
        res => {
          this.additionalFilesService.refreshListForOthers();
          this.toastr.warning('Deleted successfully');
          
      },
      err =>{
        console.log(err);
      });;
      
    }
  }

  reloadFiles(){
    this.othersFiles=[];
    this.getOthersFilesById(this.others_id);
    const component = this;
    setTimeout(function(){
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var othersFiles = component.othersFiles;
      var tr = '';
      var filePerUrl = []
      for(var i = 0; i < othersFiles.length; i ++){
        filePerUrl[i] = component.downloadFile(othersFiles[i])
        tr = tr + 
            "<tr>" +
              "<td>" + othersFiles[i].fileName + "</td>" +
              "<td>" +
                  "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + othersFiles[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                      "<img src='../assets/images/download.png' width='15' height='15'>" +
                  "</a>" +
                  "<button type='button' class='btn btn-sm btn-danger' id='deleteFiles' data-id='"+othersFiles[i].id+"'>" +
                      "<img src='../assets/images/delete.png' width='15' height='15'>" +
                  "</button>"+
                "</td>" +
              "</tr>"

      }
      $('#filesTable').append(tableBodyOpen + tr + tableBodyClose);
    },1000)
  }

  downloadFile(othersFile){
    var binaryString = '';
    for(var a = 0; a < othersFile.fileBinary.length; a++) {
      binaryString = binaryString + othersFile.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  updateRecord(form:NgForm){
    this.othersService.formData.updatedBy = this.loginUser;
    this.othersService.formData.description = this.others.description;
    console.log(this.othersService.formData);

    this.othersService.updateOthers().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';

        this.toastr.info('Updated successfully','File Updated')
        // this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadFiles()
        this.othersService.additionalFiles = [];
        this.loadSelectedFiles()
      },
      err => {
        console.log(err);
      }
    );
  }
  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    }
  }
  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile)
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "Others";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.othersService.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.othersService.additionalFiles)

    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.othersService.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.othersService.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRowOtherss'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRowOtherss',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.othersService.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.othersService.additionalFiles);
      component.loadSelectedFiles();
    });
  }

}
