import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmsOthersComponent } from './dms-others.component';

describe('DmsOthersComponent', () => {
  let component: DmsOthersComponent;
  let fixture: ComponentFixture<DmsOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmsOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmsOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
