import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { OthersService } from '../shared/others.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { Others } from '../shared/others.model';
import { Router } from '@angular/router';
import { AdditionalFilesService } from '../shared/additional-files.service';

@Component({
  selector: 'app-dms-others',
  templateUrl: './dms-others.component.html',
  styleUrls: ['./dms-others.component.css']
})
export class DmsOthersComponent implements OnInit {
  selectedFile:File = null;
  fileUrl;
  loginUser;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  
  constructor(public service:OthersService,
    private sanitizer: DomSanitizer,
    private toastr: ToastrService,
    private datePipe: DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService) { }

  ngOnInit() {
    if(localStorage.getItem('loginUser') == null){
      this.router.navigate(['login']);
    }
    $(document).ready(function() {
      setTimeout(function(){
        $('#dataTableOthers').DataTable({
          "order": [[ 1,'desc']]
        });
      }, 500)
    });

    this.resetForm();
    this.service.refreshList();
    this.loginUser = localStorage.getItem('loginUser');
    this.removeSelectedFile();
    const component = this;
    $('#dataTableOthers').on('click', '#deleteData', function(){

      var id = $(this).data('id');
      component.onDelete(id);

    }) ;

    $('#dataTableOthers').on('click', '#updateData', function(){
      var others_id = $(this).data('id');
      var others = component.service.list.find(x => x.id == others_id)
      component.populateForm(others);
    }) ;

    $('#dataTableOthers').on('click','#navigateToFiles', function(){
      var expenses_id = $(this).data('id');
      component.navigateToFiles(expenses_id);
    })
  }

  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.service.formData = {
      id: 0,
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateUpdated: new Date(),
      createdBy:'',
      updatedBy:'',
    };
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    };
    
  }

  clearForm(){
    this.resetForm();
    this.clearInputFile()
  }

  clearInputFile(){
    this.fileInput.nativeElement.value = '';
    this.service.additionalFiles = [];
    $('#removeAll').remove();
  }
  onSubmit(form:NgForm){
    if(this.service.formData.id == 0){
      this.insertRecord(form);
      
    }else{
      this.updateRecord(form);
    }
    
  }

  insertRecord(form:NgForm){
    this.service.formData.createdBy = this.loginUser;
    this.service.formData.updatedBy = this.loginUser;
    this.service.addOthers().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';
        this.resetForm(form);
        this.toastr.success('Submitted successfully','New File Uploaded')
        this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadDataTable();
      },
      err => {
        console.log(err);
      }
    );
  }

  updateRecord(form:NgForm){
    this.service.formData.updatedBy = this.loginUser;
    this.service.updateOthers().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';
        this.resetForm(form);
        this.toastr.info('Updated successfully','File Updated')
        this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadDataTable();
      },
      err => {
        console.log(err);
      }
    );
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record?" + id)){
      console.log(id)
      this.service.deleteOthers(id).subscribe(
        res => {
          this.service.refreshList();
          this.toastr.warning('Deleted successfully');
          this.reloadDataTable();
      },
      err =>{
        console.log(err);
      });;
     
    }
  }

  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "Others";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.service.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.service.additionalFiles)

    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.service.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.service.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRowLetter'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRowLetter',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.service.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.service.additionalFiles);
      component.loadSelectedFiles();
    });
  }

  downloadFile(others){
    var binaryString = '';
    for(var a = 0; a < others.fileBinary.length; a++) {
      binaryString = binaryString + others.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  populateForm(others:Others){
    this.service.formData = Object.assign({}, others);
  }

  reloadDataTable(){
    const component = this;
    setTimeout(function() { 
      $('#dataTableOthers').DataTable().destroy()
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var others = component.service.list;
      var tr = '';
      var filePerUrl = [];
      
      for(var i = 0; i < others.length; i ++){
        var formattedDate = component.datePipe.transform(others[i].dateUpdated, 'yyyy/MM/dd')
        // filePerUrl[i] = component.downloadFile(others[i])
        tr = tr +
          "<tr>" +
            "<td>" + others[i].description + "</td>" +
            "<td>" + formattedDate + "</td>" + 
            "<td>" +
                // "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + others[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                //     "<i class='fas fa-download'></i>" +
                // "</a>" +
                // "<button type='button' class='btn btn-sm btn-warning' data-toggle='modal' data-target='#exampleModal' id='updateData' data-id='" + others[i].id + "'>" +
                //     "<i class='far fa-edit fa-lg' ></i>" +
                // "</button>" +
                "<a  id='navigateToFiles' data-id='"+ others[i].id +"' routerLinkActive='active' class='btn btn-sm btn-info' ><img src='../assets/images/view.png' width='15' height='15'></a>" +
                "<button type='button' class='btn btn-sm btn-danger' id='deleteData' data-id='" + others[i].id + "'>" +
                    "<img src='../assets/images/delete.png' width='15' height='15'>" +
                "</button>" +
            "</td>" +
          "</tr>"
        
          // onclick='" + this.onDelete(expenses[i].id) + "'
        
      }

      $('#dataTableOthers').append(tableBodyOpen + tr + tableBodyClose); 
      $('#dataTableOthers').DataTable({
        "order": [[ 1,'desc']]
      })
    }, 1000);
  }

  navigateToFiles(id){
    this.router.navigate(['Others',id, 'Files']);
  }
}
