import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
declare var $
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  UserName;
  Password;
  users = [
    {'username': 'admin', 'password': 'admin', 'name': 'Administrator'},
    {'username': 'user', 'password': 'password123', 'name': 'Default User'}
  ]
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form:NgForm){
    if(this.users[0].username == this.UserName && this.users[0].password == this.Password){
      this.router.navigate(['Expenses']);
      localStorage.setItem('loginUser', this.users[0].name);

    }else if(this.users[1].username == this.UserName && this.users[1].password == this.Password){
      this.router.navigate(['Expenses']);
      localStorage.setItem('loginUser', this.users[1].name);;
    }else{
      alert('Incorrect Username or Password')
    }
  }
}
