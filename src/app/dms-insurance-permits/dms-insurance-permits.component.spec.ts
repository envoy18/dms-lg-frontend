import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmsInsurancePermitsComponent } from './dms-insurance-permits.component';

describe('DmsInsurancePermitsComponent', () => {
  let component: DmsInsurancePermitsComponent;
  let fixture: ComponentFixture<DmsInsurancePermitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmsInsurancePermitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmsInsurancePermitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
