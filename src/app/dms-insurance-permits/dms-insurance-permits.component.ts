import { Component, OnInit, Renderer2, Inject, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT, DatePipe } from '@angular/common';
import { InsurancePermitsService } from '../shared/insurance-permits.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { InsurancePermits } from '../shared/insurance-permits.model';
import { Router } from '@angular/router';
import { AdditionalFilesService } from '../shared/additional-files.service';
declare var $;

@Component({
  selector: 'app-dms-insurance-permits',
  templateUrl: './dms-insurance-permits.component.html',
  styleUrls: ['./dms-insurance-permits.component.css']
})
export class DmsInsurancePermitsComponent implements OnInit {
  selectedFile:File = null;
  fileUrl;
  getDate = new Date();
  currentDate ='';
  loginUser
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  @ViewChild('datePicker', {static: false}) datePicker: ElementRef;

  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public service: InsurancePermitsService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private datePipe: DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService) { } 
  

  ngOnInit() {
    if(localStorage.getItem('loginUser') == null){
      this.router.navigate(['login']);
    }
    $(document).ready(function() {
      setTimeout(function(){
        $('#dataTableInsurancePermit').DataTable({
          "order": [[ 2,'desc']]
        });
      }, 500)
    });
    this.resetForm();
    this.service.refreshList();
    this.currentDate = this.datePipe.transform(this.getDate, 'yyyy-MM-dd');
    this.loginUser = localStorage.getItem('loginUser');
    this.removeSelectedFile();
    const component = this;
    $('#dataTableInsurancePermit').on('click', '#deleteData', function(){

      var id = $(this).data('id');
      component.onDelete(id);

    }) ;

    $('#dataTableInsurancePermit').on('click', '#updateData', function(){
      component.clearInputFile();
      var insurancePermit_id = $(this).data('id');
      var insurancePermit = component.service.list.find(x => x.id == insurancePermit_id)
      component.populateForm(insurancePermit);
    }) ;

    $('#dataTableInsurancePermit').on('click','#navigateToFiles', function(){
      var expenses_id = $(this).data('id');
      component.navigateToFiles(expenses_id);
    })
  }

  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.service.formData = {
      id: 0,
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateExpiration:new Date(0),
      dateUpdated: new Date(),
      createdBy:'',
      updatedBy:'',
    };
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    };

  }

  onDateChange(event){
    console.log(event.target.value)
    this.service.formData.dateExpiration = event.target.value;

  }

  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "InsurancePermits";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.service.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.service.additionalFiles)
    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.service.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.service.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRowLetter'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRowLetter',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.service.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.service.additionalFiles);
      component.loadSelectedFiles();
    });
  }
  onSubmit(form:NgForm){
    if(this.service.formData.id == 0){
      this.insertRecord(form);
    }else{
      this.updateRecord(form);
    }
  }

  insertRecord(form:NgForm){
    this.service.formData.createdBy = this.loginUser;
    this.service.formData.updatedBy = this.loginUser;
    this.service.addInsurancePermits().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';
        this.resetForm(form);
        this.toastr.success('Submitted successfully','New File Uploaded')
        this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadDataTable();
      },
      err => {
        console.log(err);
      }
    );
  }

  updateRecord(form:NgForm){
    this.service.formData.updatedBy = this.loginUser;
    this.service.updateInsurancePermits().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';
        this.resetForm(form);
        this.toastr.info('Updated successfully','File Updated')
        this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadDataTable();
      },
      err => {
        console.log(err);
      }
    );
  }

  populateForm(insurancePermit:InsurancePermits){
    this.clearInputFile()
    this.service.formData = Object.assign({}, insurancePermit);
  }

  clearInputFile(){
    this.fileInput.nativeElement.value = '';
    this.datePicker.nativeElement.value = '';
    this.service.additionalFiles = [];
    $('#removeAll').remove();
  }

  clearForm(){
    this.resetForm();
    this.clearInputFile()
    
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record?"+id)){
      console.log(id)
      this.service.deleteInsurancePermits(id).subscribe(
        res => {
          this.service.refreshList();
          this.toastr.warning('Deleted successfully');
          this.reloadDataTable();
      },
      err =>{
        console.log(err);
      });;
     
    }
  }

  downloadFile(insurancePermit){
    var binaryString = '';
    for(var a = 0; a < insurancePermit.fileBinary.length; a++) {
      binaryString = binaryString + insurancePermit.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  reloadDataTable(){
    const component = this;
    setTimeout(function() { 
      $('#dataTableInsurancePermit').DataTable().destroy()
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var insurancePermit = component.service.list;
      var tr = '';
      var filePerUrl = [];
      
      for(var i = 0; i < insurancePermit.length; i ++){
        var formattedDate = component.datePipe.transform(insurancePermit[i].dateUpdated, 'yyyy/MM/dd')
        var dueDate = component.datePipe.transform(insurancePermit[i].dateExpiration)
        filePerUrl[i] = component.downloadFile(insurancePermit[i])
        tr = tr +
          "<tr>" +
            "<td>" + insurancePermit[i].description + "</td>" +
            "<td>" + dueDate + "</td>" + 
            "<td>" + formattedDate + "</td>" + 
            "<td>" +
                // "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + insurancePermit[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                //     "<i class='fas fa-download'></i>" +
                // "</a>" +
                // "<button type='button' class='btn btn-sm btn-warning' data-toggle='modal' data-target='#exampleModal' id='updateData' data-id='" + insurancePermit[i].id + "'>" +
                //     "<i class='far fa-edit fa-lg' ></i>" +
                // "</button>" +
                "<a  id='navigateToFiles' data-id='"+ insurancePermit[i].id +"' routerLinkActive='active' class='btn btn-sm btn-info' ><img src='../assets/images/view.png' width='15' height='15'></a>" +
                "<button type='button' class='btn btn-sm btn-danger' id='deleteData' data-id='" + insurancePermit[i].id + "'>" +
                    "<img src='../assets/images/delete.png' width='15' height='15'>" +
                "</button>" +
            "</td>" +
          "</tr>"
        
          // onclick='" + this.onDelete(expenses[i].id) + "'
        
      }

      $('#dataTableInsurancePermit').append(tableBodyOpen + tr + tableBodyClose); 
      $('#dataTableInsurancePermit').DataTable({
        "order": [[ 2,'desc']]
      })
    }, 1000);
  }

  navigateToFiles(id){
    this.router.navigate(['InsurancePermits',id, 'Files']);
  }
}

