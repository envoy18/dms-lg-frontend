import { Component, OnInit, Renderer2, Inject, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT, CurrencyPipe, DatePipe } from '@angular/common';

import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AdditionalFilesService } from 'src/app/shared/additional-files.service';
import { InsurancePermitsService } from 'src/app/shared/insurance-permits.service';
import { NgForm } from '@angular/forms';
declare var $;
@Component({
  selector: 'app-dms-insurance-permits-files',
  templateUrl: './dms-insurance-permits-files.component.html',
  styleUrls: ['./dms-insurance-permits-files.component.css']
})
export class DmsInsurancePermitsFilesComponent implements OnInit {
  insurancePermits_id;
  insurancePermits:any = [];
  insurancePermitsFiles =[];
  fileUrl;
  loginUser;
  selectedFile:File;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public insurancePermitsService: InsurancePermitsService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private currencyPipe: CurrencyPipe,
    private datePipe:DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loginUser = localStorage.getItem('loginUser');
    const component = this;
    this.insurancePermitsService.refreshList();
    this.additionalFilesService.refreshListForInsurancePermits();
    this.insurancePermits_id = this.route.snapshot.paramMap.get('id');
    this.insurancePermitsService.getInsurancePermitsByID(this.insurancePermits_id).subscribe((res)=>{
            this.insurancePermits = res;
            this.insurancePermits.id
            
        });
    this.getInsurancePermitsFilesById(this.insurancePermits_id);
    this.removeSelectedFile();
    $('#filesTable').on('click', '#deleteFiles', function(){
      var id = $(this).data('id');
      component.onDelete(id);
    })
  }

  populateForm(insurancePermits:any){
    this.insurancePermitsService.formData = Object.assign({}, this.insurancePermits)
    this.insurancePermitsService.additionalFiles = [];
    this.loadSelectedFiles()
    
  }

  getInsurancePermitsFilesById(id){
    this.additionalFilesService.refreshListForInsurancePermits();
    const component = this;
    setTimeout(function(){
      var insurancePermitsFiles = component.additionalFilesService.additionalFilesListInsurancePermits
      for(var i = 0; i < insurancePermitsFiles.length; i ++){
        if(insurancePermitsFiles[i].files_Id == id){
          component.insurancePermitsFiles.push(insurancePermitsFiles[i])
          
        }
      }
      console.log(component.insurancePermitsFiles)
    },1000)
    
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record? " + id)){
      this.reloadFiles();
      this.additionalFilesService.deleteInsurancePermitsAdditionalFileById(id).subscribe(
        res => {
          this.additionalFilesService.refreshListForInsurancePermits();
          this.toastr.warning('Deleted successfully');
          
      },
      err =>{
        console.log(err);
      });;
      
    }
  }

  reloadFiles(){
    this.insurancePermitsFiles=[];
    this.getInsurancePermitsFilesById(this.insurancePermits_id);
    const component = this;
    setTimeout(function(){
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var insurancePermitsFiles = component.insurancePermitsFiles;
      var tr = '';
      var filePerUrl = []
      for(var i = 0; i < insurancePermitsFiles.length; i ++){
        filePerUrl[i] = component.downloadFile(insurancePermitsFiles[i])
        tr = tr + 
            "<tr>" +
              "<td>" + insurancePermitsFiles[i].fileName + "</td>" +
              "<td>" +
                  "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + insurancePermitsFiles[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                      "<img src='../assets/images/download.png' width='15' height='15'>" +
                  "</a>" +
                  "<button type='button' class='btn btn-sm btn-danger' id='deleteFiles' data-id='"+insurancePermitsFiles[i].id+"'>" +
                      "<img src='../assets/images/delete.png' width='15' height='15'>" +
                  "</button>"+
                "</td>" +
              "</tr>"

      }
      $('#filesTable').append(tableBodyOpen + tr + tableBodyClose);
    },1000)
  }

  downloadFile(insurancePermitsFile){
    var binaryString = '';
    for(var a = 0; a < insurancePermitsFile.fileBinary.length; a++) {
      binaryString = binaryString + insurancePermitsFile.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  updateRecord(form:NgForm){
    this.insurancePermitsService.formData.updatedBy = this.loginUser;
    this.insurancePermitsService.formData.description = this.insurancePermits.description;
    console.log(this.insurancePermitsService.formData);

    this.insurancePermitsService.updateInsurancePermits().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';

        this.toastr.info('Updated successfully','File Updated')
        // this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadFiles()
        this.insurancePermitsService.additionalFiles = [];
        this.loadSelectedFiles()
      },
      err => {
        console.log(err);
      }
    );
  }
  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    }
  }
  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile)
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "InsurancePermits";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.insurancePermitsService.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.insurancePermitsService.additionalFiles)

    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.insurancePermitsService.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.insurancePermitsService.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRowInsurancePermitss'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRowInsurancePermitss',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.insurancePermitsService.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.insurancePermitsService.additionalFiles);
      component.loadSelectedFiles();
    });
  }

  onDateChange(event){
    console.log(event.target.value)
    this.insurancePermits.dateExpiration = event.target.value;
    this.insurancePermitsService.formData.dateExpiration = this.insurancePermits.dateExpiration
  }
}
