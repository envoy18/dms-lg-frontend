export class Letter {
    id:number;
    fileName:string;
    fileSize:number;
    fileType:string;
    fileBinary:any[];
    description:string;
    dateUpdated:Date;
    createdBy:string;
    updatedBy:string;
}
