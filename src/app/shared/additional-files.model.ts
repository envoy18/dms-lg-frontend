export class AdditionalFiles {
    id:number;
    files_Id:number;
    category:string;
    fileName:string;
    fileSize:number;
    fileType:string;
    fileBinary:any[];
    description:string;
    dateCreated:Date;
    createdBy:string;

}
