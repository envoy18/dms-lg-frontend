import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Others } from './others.model';

@Injectable({
  providedIn: 'root'
})
export class OthersService {
  formData: Others;
  readonly rootURL ="https://localhost:44379/api/others";
  list: Others[];
  combine;
  additionalFiles=[];
  constructor(private http:HttpClient) { }

  addOthers(){
    this.combine = {
      "others":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/create", this.combine)
  }

  refreshList(){
    return this.http.get(this.rootURL + "/list")
    .toPromise()
    .then(res => this.list = res as Others[]);
  }

  deleteOthers(id){
    return this.http.get(this.rootURL + "/delete/"+id)
  }

  updateOthers(){
    this.combine = {
      "others":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/update", this.combine)
  }

  getOthersByID(id){
    return this.http.get(this.rootURL + "/GetById/"+id);
  }
}
