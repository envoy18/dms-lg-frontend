import { Injectable } from '@angular/core';
import { Expenses } from './expenses.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DateFilter } from './date-filter.model';
import { AdditionalFiles } from './additional-files.model';

@Injectable({
  providedIn: 'root'
})
export class ExpensesService {
  formData: Expenses;
  formDataDateFilter: DateFilter;
  readonly rootURL ="https://localhost:44379/api/expenses";
  list: Expenses[];
  listByDate: Expenses[];
  combine;
  additionalFiles=[];
  constructor(private http:HttpClient) { }
 
  
  addExpenses(){

    

    this.combine = {
      "expenses":this.formData,
      
        "additional":this.additionalFiles
      }
       
    console.log(this.formData)
    return this.http.post(this.rootURL + "/create", this.combine)
  }

  refreshList(){
    return this.http.get(this.rootURL + "/list")
    .toPromise()
    .then(res => this.list = res as Expenses[]);
  }

  deleteExpenses(id){
    return this.http.get(this.rootURL + "/delete/"+id)
  }

  updateExpenses(){
    this.combine = {
      "expenses":this.formData,
      "additional":this.additionalFiles
    }
    return this.http.post(this.rootURL + "/update", this.combine)
  }

  refreshListByDate(){

    return this.http.post(this.rootURL + "/listbydate", this.formDataDateFilter)
    .toPromise()
    .then(res => this.listByDate = res as Expenses[]);
  }

  getExpensesByID(id){
    return this.http.get(this.rootURL + "/GetById/"+id);
  }
  
}
