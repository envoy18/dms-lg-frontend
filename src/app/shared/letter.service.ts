import { Injectable } from '@angular/core';
import { Letter } from './letter.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LetterService {
  formData: Letter;
  readonly rootURL ="https://localhost:44379/api/letter";
  list: Letter[];
  combine;
  additionalFiles=[];
  constructor(private http:HttpClient) { }

  addLetter(){
    this.combine = {
      "letter":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/create", this.combine)
  }

  refreshList(){
    return this.http.get(this.rootURL + "/list")
    .toPromise()
    .then(res => this.list = res as Letter[]);
  }

  deleteLetter(id){
    return this.http.get(this.rootURL + "/delete/"+id)
  }

  updateLetter(){
    this.combine = {
      "letter":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/update", this.combine)
  }

  getLetterByID(id){
    return this.http.get(this.rootURL + "/GetById/"+id);
  }
}
