import { Injectable } from '@angular/core';
import { InsurancePermits } from './insurance-permits.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InsurancePermitsService {
  formData: InsurancePermits
  readonly rootURL ="https://localhost:44379/api/insurancepermit";
  list: InsurancePermits[];
  expiringApplicationList: InsurancePermits[];
  combine;
  additionalFiles=[];
  constructor(private http:HttpClient) { }

  addInsurancePermits(){
    this.combine = {
      "insurancePermit":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/create", this.combine)
  }

  refreshList(){
    return this.http.get(this.rootURL + "/list")
    .toPromise()
    .then(res => this.list = res as InsurancePermits[]);
  }

  deleteInsurancePermits(id){
    return this.http.get(this.rootURL + "/delete/"+id)
  }

  updateInsurancePermits(){
    this.combine = {
      "insurancePermit":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/update", this.combine)
  }

  expiringApplication(){
    return this.http.get(this.rootURL + "/expiring")
    .toPromise()
    .then(res => this.expiringApplicationList = res as InsurancePermits[]);
  }

  getInsurancePermitsByID(id){
    return this.http.get(this.rootURL + "/GetById/"+id);
  }
}
