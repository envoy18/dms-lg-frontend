import { Injectable } from '@angular/core';
import { AdditionalFiles } from './additional-files.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdditionalFilesService {
  formData: AdditionalFiles;
  readonly rootURL ="https://localhost:44379/api/additionalfiles";
  list: AdditionalFiles[];
  additionalFilesListExpenses: AdditionalFiles[]
  additionalFilesListLetters: AdditionalFiles[]
  additionalFilesListSalesReports: AdditionalFiles[]
  additionalFilesListInsurancePermits: AdditionalFiles[]
  additionalFilesListOthers: AdditionalFiles[]
  constructor(private http:HttpClient) { }

  refreshList(){
    return this.http.get(this.rootURL + "/list")
    .toPromise()
    .then(res => this.list = res as AdditionalFiles[]);
  }

  refreshListForExpenses(){
    return this.http.get(this.rootURL + "/listadditionalfilesforexpenses")
    .toPromise()
    .then(res => this.additionalFilesListExpenses = res as AdditionalFiles[]);
  }
  
  refreshListForLetters(){
    return this.http.get(this.rootURL + "/listadditionalfilesforletters")
    .toPromise()
    .then(res => this.additionalFilesListLetters = res as AdditionalFiles[]);
  }

  refreshListForSalesReports(){
    return this.http.get(this.rootURL + "/listadditionalfilesforsalesreport")
    .toPromise()
    .then(res => this.additionalFilesListSalesReports = res as AdditionalFiles[]);
  }

  refreshListForInsurancePermits(){
    return this.http.get(this.rootURL + "/listadditionalfilesforinsurancepermits")
    .toPromise()
    .then(res => this.additionalFilesListInsurancePermits = res as AdditionalFiles[]);
  }

  refreshListForOthers(){
    return this.http.get(this.rootURL + "/listadditionalfilesforothers")
    .toPromise()
    .then(res => this.additionalFilesListOthers = res as AdditionalFiles[]);
  }

  deleteExpensesAdditionalFileById(id){
    return this.http.get(this.rootURL + "/deleteExpensesById/"+id)
  }

  deleteLettersAdditionalFileById(id){
    return this.http.get(this.rootURL + "/deleteLettersById/"+id)
  }

  deleteSalesReportsAdditionalFileById(id){
    return this.http.get(this.rootURL + "/deleteSalesReportById/"+id)
  }

  deleteInsurancePermitsAdditionalFileById(id){
    return this.http.get(this.rootURL + "/deleteInsurancePermitsById/"+id)
  }

  deleteOthersAdditionalFileById(id){
    return this.http.get(this.rootURL + "/deleteOthersById/"+id)
  }
}
