import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SalesReport } from './sales-report.model';

@Injectable({
  providedIn: 'root'
})
export class SalesReportService {
  formData: SalesReport
  readonly rootURL ="https://localhost:44379/api/salesreport";
  list: SalesReport[];
  combine;
  additionalFiles=[];
  constructor(private http:HttpClient) { }

  addSalesReport(){
    this.combine = {
      "salesReport":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/create", this.combine)
  }

  refreshList(){
    return this.http.get(this.rootURL + "/list")
    .toPromise()
    .then(res => this.list = res as SalesReport[]);
  }

  deleteSalesReport(id){
    return this.http.get(this.rootURL + "/delete/"+id)
  }

  updateSalesReport(){
    this.combine = {
      "salesReport":this.formData,
      "additional":this.additionalFiles
      }
    return this.http.post(this.rootURL + "/update", this.combine)
  }

  getSalesReportByID(id){
    return this.http.get(this.rootURL + "/GetById/"+id);
  }
}
