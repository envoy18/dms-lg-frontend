export class InsurancePermits {
    id:number;
    fileName:string;
    fileSize:number;
    fileType:string;
    fileBinary:any[];
    description:string;
    dateExpiration:Date;
    dateUpdated:Date;
    createdBy:string;
    updatedBy:string;
}
