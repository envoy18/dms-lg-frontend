export class Expenses {
    id:number;
    fileName:string;
    fileSize:number;
    fileType:string;
    fileBinary:any[];
    description:string;
    amount:number;
    dateCreated:Date;
    dateUpdated:Date;
    createdBy:string;
    updatedBy:string;
}
