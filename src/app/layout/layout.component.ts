import { Component, OnInit, Renderer2, Inject } from '@angular/core';

import { DOCUMENT, DatePipe } from '@angular/common';
import { InsurancePermitsService } from '../shared/insurance-permits.service';

declare var $;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  notifCount;
  currentDate;
  loginUser;
  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public insurancePermitService: InsurancePermitsService,
    private datePipe: DatePipe) { } 

  ngOnInit() {
   $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    });
    this.currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.insurancePermitService.expiringApplication();
    this.getNotifCount();
    this.loginUser = localStorage.getItem('loginUser');
  }

  getNotifCount(){
    const component = this;
    setTimeout(function(){
      this.notifCount = component.insurancePermitService.expiringApplicationList.length;

      console.log(this.notifCount)
      $("#counter").append(this.notifCount);
    }, 3000)
   
  }

  clearSession(){
    localStorage.clear();
  }
}