import { Component, OnInit, Renderer2, Inject, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT, DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { SalesReportService } from '../shared/sales-report.service';
import { ToastrService } from 'ngx-toastr';
import { SalesReport } from '../shared/sales-report.model';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AdditionalFilesService } from '../shared/additional-files.service';
declare var $;

@Component({
  selector: 'app-dms-salesreport',
  templateUrl: './dms-salesreport.component.html',
  styleUrls: ['./dms-salesreport.component.css']
})
export class DmsSalesreportComponent implements OnInit {
  selectedFile:File = null;
  fileUrl;
  loginUser;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public service: SalesReportService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private datePipe: DatePipe,
    private router : Router,
    public additionalFilesService: AdditionalFilesService) { } 
  

  ngOnInit() {
    if(localStorage.getItem('loginUser') == null){
      this.router.navigate(['login']);
    }
    $(document).ready(function() {
      setTimeout(function(){
        $('#dataTableSalesReport').DataTable({
          "order": [[ 1,'desc']]
        });
      }, 500)
    });
    this.resetForm();
    this.service.refreshList();
    this.loginUser = localStorage.getItem('loginUser');
    this.removeSelectedFile();
    const component = this;
    $('#dataTableSalesReport').on('click', '#deleteData', function(){

      var id = $(this).data('id');
      component.onDelete(id);

    }) ;

    $('#dataTableSalesReport').on('click', '#updateData', function(){
      var salesReport_id = $(this).data('id');
      var salesReport = component.service.list.find(x => x.id == salesReport_id)
      component.populateForm(salesReport);
    }) ;

    $('#dataTableSalesReport').on('click','#navigateToFiles', function(){
      var expenses_id = $(this).data('id');
      component.navigateToFiles(expenses_id);
    })
  }

  
  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.service.formData = {
      id: 0,
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateUpdated:new Date(),
      createdBy:'',
      updatedBy:'',
    };
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    };
  }

  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "SalesReport";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.service.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.service.additionalFiles)
    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.service.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.service.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRowLetter'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRowLetter',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.service.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.service.additionalFiles);
      component.loadSelectedFiles();
    });
  }
  onSubmit(form:NgForm){
    if(this.service.formData.id == 0){
      this.insertRecord(form);
      
    }else{
      this.updateRecord(form);
    }
    
  }

  insertRecord(form:NgForm){
    this.service.formData.createdBy = this.loginUser;
    this.service.formData.updatedBy = this.loginUser;
    this.service.addSalesReport().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';
        this.resetForm(form);
        this.toastr.success('Submitted successfully','New File Uploaded')
        this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadDataTable();
      },
      err => {
        console.log(err);
      }
    );
  }

  updateRecord(form:NgForm){
    this.service.formData.updatedBy = this.loginUser;
    this.service.updateSalesReport().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';
        this.resetForm(form);
        this.toastr.info('Updated successfully','File Updated')
        this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadDataTable();
      },
      err => {
        console.log(err);
      }
    );
  }

  populateForm(salesReport:SalesReport){
    this.clearInputFile()
    this.service.formData = Object.assign({}, salesReport);
  }

  clearForm(){
    this.resetForm();
    this.clearInputFile()
  }

  clearInputFile(){
    this.fileInput.nativeElement.value = '';
    this.service.additionalFiles = [];
    $('#removeAll').remove();
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record?"+id)){
      console.log(id)
      this.service.deleteSalesReport(id).subscribe(
        res => {
          this.service.refreshList();
          this.toastr.warning('Deleted successfully');
          this.reloadDataTable();
      },
      err =>{
        console.log(err);
      });;
     
    }
  }

  downloadFile(salesReport){
    var binaryString = '';
    for(var a = 0; a < salesReport.fileBinary.length; a++) {
      binaryString = binaryString + salesReport.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  reloadDataTable(){
    const component = this;
    setTimeout(function() { 
      $('#dataTableSalesReport').DataTable().destroy()
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var salesReport = component.service.list;
      var tr = '';
      var filePerUrl = [];
      
      for(var i = 0; i < salesReport.length; i ++){
        var formattedDate = component.datePipe.transform(salesReport[i].dateUpdated, 'yyyy/MM/dd')
        filePerUrl[i] = component.downloadFile(salesReport[i])
        tr = tr +
          "<tr>" +
            "<td>" + salesReport[i].description + "</td>" +
            "<td>" + formattedDate + "</td>" + 
            "<td>" +
                // "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + salesReport[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                //     "<i class='fas fa-download'></i>" +
                // "</a>" +
                // "<button type='button' class='btn btn-sm btn-warning' data-toggle='modal' data-target='#exampleModal' id='updateData' data-id='" + salesReport[i].id + "'>" +
                //     "<i class='far fa-edit fa-lg' ></i>" +
                // "</button>" +
                "<a  id='navigateToFiles' data-id='"+ salesReport[i].id +"' routerLinkActive='active' class='btn btn-sm btn-info' ><img src='../assets/images/view.png' width='15' height='15'></a>" +
                "<button type='button' class='btn btn-sm btn-danger' id='deleteData' data-id='" + salesReport[i].id + "'>" +
                    "<img src='../assets/images/delete.png' width='15' height='15'>" +
                "</button>" +
            "</td>" +
          "</tr>"
        
          // onclick='" + this.onDelete(expenses[i].id) + "'
        
      }

      $('#dataTableSalesReport').append(tableBodyOpen + tr + tableBodyClose); 
      $('#dataTableSalesReport').DataTable({
        "order": [[ 1,'desc']]
      })
    }, 1000);
  }
  navigateToFiles(id){
    this.router.navigate(['SalesReport',id, 'Files']);
  }
}
