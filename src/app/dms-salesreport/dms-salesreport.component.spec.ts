import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmsSalesreportComponent } from './dms-salesreport.component';

describe('DmsSalesreportComponent', () => {
  let component: DmsSalesreportComponent;
  let fixture: ComponentFixture<DmsSalesreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmsSalesreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmsSalesreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
