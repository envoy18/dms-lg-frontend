import { Component, OnInit, Renderer2, Inject, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT, CurrencyPipe, DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AdditionalFilesService } from 'src/app/shared/additional-files.service';
import { SalesReportService } from 'src/app/shared/sales-report.service';
import { NgForm } from '@angular/forms';
declare var $;
@Component({
  selector: 'app-dms-salesreport-files',
  templateUrl: './dms-salesreport-files.component.html',
  styleUrls: ['./dms-salesreport-files.component.css']
})
export class DmsSalesreportFilesComponent implements OnInit {
  salesReport_id;
  salesReport:any = [];
  salesReportFiles =[];
  fileUrl;
  loginUser;
  selectedFile:File;
  @ViewChild('fileInput',{static: false}) fileInput:ElementRef;
  @ViewChild('closeModal', {static: false}) closeModal: ElementRef;
  constructor(private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public salesReportService: SalesReportService,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer,
    private currencyPipe: CurrencyPipe,
    private datePipe:DatePipe,
    private router: Router,
    public additionalFilesService: AdditionalFilesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loginUser = localStorage.getItem('loginUser');
    const component = this;
    this.salesReportService.refreshList();
    this.additionalFilesService.refreshListForSalesReports();
    this.salesReport_id = this.route.snapshot.paramMap.get('id');
    this.salesReportService.getSalesReportByID(this.salesReport_id).subscribe((res)=>{
            this.salesReport = res;
            this.salesReport.id
            
        });
    this.getSalesReportFilesById(this.salesReport_id);
    this.removeSelectedFile();
    $('#filesTable').on('click', '#deleteFiles', function(){
      var id = $(this).data('id');
      component.onDelete(id);
    })
  }
  populateForm(salesReport:any){
    this.salesReportService.formData = Object.assign({}, this.salesReport)
    this.salesReportService.additionalFiles = [];
    this.loadSelectedFiles()
    
  }

  getSalesReportFilesById(id){
    this.additionalFilesService.refreshListForSalesReports();
    const component = this;
    setTimeout(function(){
      var salesReportFiles = component.additionalFilesService.additionalFilesListSalesReports
      for(var i = 0; i < salesReportFiles.length; i ++){
        if(salesReportFiles[i].files_Id == id){
          component.salesReportFiles.push(salesReportFiles[i])
          
        }
      }
      console.log(component.salesReportFiles)
    },1000)
    
  }

  onDelete(id){
    if(confirm("Are you sure to delete this record? " + id)){
      this.reloadFiles();
      this.additionalFilesService.deleteSalesReportsAdditionalFileById(id).subscribe(
        res => {
          this.additionalFilesService.refreshListForSalesReports();
          this.toastr.warning('Deleted successfully');
          
      },
      err =>{
        console.log(err);
      });;
      
    }
  }

  reloadFiles(){
    this.salesReportFiles=[];
    this.getSalesReportFilesById(this.salesReport_id);
    const component = this;
    setTimeout(function(){
      $('tbody').remove();
      var tableBodyOpen = "<tbody>";
      var tableBodyClose = "</tbody>"
      var salesReportFiles = component.salesReportFiles;
      var tr = '';
      var filePerUrl = []
      for(var i = 0; i < salesReportFiles.length; i ++){
        filePerUrl[i] = component.downloadFile(salesReportFiles[i])
        tr = tr + 
            "<tr>" +
              "<td>" + salesReportFiles[i].fileName + "</td>" +
              "<td>" +
                  "<a href='" + filePerUrl[i].changingThisBreaksApplicationSecurity + "' download='" + salesReportFiles[i].fileName + "' class='btn btn-sm btn-info' id='donwloadFile'>" +
                      "<img src='../assets/images/download.png' width='15' height='15'>" +
                  "</a>" +
                  "<button type='button' class='btn btn-sm btn-danger' id='deleteFiles' data-id='"+salesReportFiles[i].id+"'>" +
                      "<img src='../assets/images/delete.png' width='15' height='15'>" +
                  "</button>"+
                "</td>" +
              "</tr>"

      }
      $('#filesTable').append(tableBodyOpen + tr + tableBodyClose);
    },1000)
  }

  downloadFile(salesReportFile){
    var binaryString = '';
    for(var a = 0; a < salesReportFile.fileBinary.length; a++) {
      binaryString = binaryString + salesReportFile.fileBinary[a];
    }
    var binary = atob(binaryString);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    var bytes = new Uint8Array(array);
    var blob = new Blob([bytes], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  updateRecord(form:NgForm){
    this.salesReportService.formData.updatedBy = this.loginUser;
    this.salesReportService.formData.description = this.salesReport.description;
    console.log(this.salesReportService.formData);

    this.salesReportService.updateSalesReport().subscribe(
      res => {
        this.fileInput.nativeElement.value = '';

        this.toastr.info('Updated successfully','File Updated')
        // this.service.refreshList();
        this.closeModal.nativeElement.click();
        this.reloadFiles()
        this.salesReportService.additionalFiles = [];
        this.loadSelectedFiles()
      },
      err => {
        console.log(err);
      }
    );
  }
  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.additionalFilesService.formData = {
      id: 0,
      category:'',
      fileName:'',
      fileSize: 0,
      fileType:'',
      description:'',
      fileBinary:[],
      dateCreated: new Date(),
      createdBy:'',
      files_Id:0
    }
  }
  onFileChange(event){
    this.resetForm();
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile)
    this.additionalFilesService.formData.fileName = this.selectedFile.name;
    this.additionalFilesService.formData.fileSize = this.selectedFile.size;
    this.additionalFilesService.formData.fileType = this.selectedFile.type;
    this.additionalFilesService.formData.category = "SalesReport";
    this.additionalFilesService.formData.createdBy = this.loginUser;
    var reader = new FileReader();
    var fileByteArray = [];
    reader.readAsArrayBuffer(this.selectedFile);
    reader.onload = function (evt:any) {
      if (evt.target.readyState == FileReader.DONE) {
        var arrayBuffer = evt.target.result,
             array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < array.length; i++) {
             fileByteArray.push(array[i]);
        }
      }
    }
    this.additionalFilesService.formData.fileBinary = fileByteArray;
    this.salesReportService.additionalFiles.push(this.additionalFilesService.formData)
    console.log(this.salesReportService.additionalFiles)

    this.loadSelectedFiles();
  }

  loadSelectedFiles(){
    $('#removeAll').remove();    
    var content ='';            
    for(var x = 0 ; x < this.salesReportService.additionalFiles.length; x++){
       content = content +
              "<div class='row' id='tableRow'>" +
                "<div class='col-md-10' >"+
                   this.salesReportService.additionalFiles[x].fileName + 
                "</div>" +
                "<div class='col'>"+
                    "<button class='btn btn-sm btn-danger' data-id='"+x+"' id='selectedFileRowSalesReports'>&#8722;</button>"+
                "</div>"+
              "</div>";
    }
    $('#selectedFiles').append("<div id='removeAll'>"+content+"</div>");
    
  }

  removeSelectedFile(){
    const component = this;
    $('body').on('click','#selectedFileRowSalesReports',function(){
      // $(this).closest('#tableRow').remove();
      var id = $(this).data('id');
      component.salesReportService.additionalFiles.splice(id,1);
      console.log(id);
      console.log(component.salesReportService.additionalFiles);
      component.loadSelectedFiles();
    });
  }

}
